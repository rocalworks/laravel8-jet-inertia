Just created this out of spite of countless trials and errors on creating a project that suits my taste. 

## Project Contents
- Laravel 8 
- Laravel Jetstream (Inertia stack)

## Additional Modification 
- Extracting Jetstream routes to routes/jetstream.php for easier access
- Add Buefy for UI (too lazy to configure using TailwindCSS)